package com.example.ti.progaming;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.transition.Scene;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.pkmmte.pkrss.Article;
import com.pkmmte.pkrss.Callback;
import com.pkmmte.pkrss.PkRSS;

import java.util.ArrayList;
import java.util.List;

public class nav_drawer extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, Callback {

    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private RecyclerAdapter adapter;
    private List<Article> list = new ArrayList<>();
    private Scene scene1;
    private Scene scene2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        adapter = new RecyclerAdapter(list);
        recyclerView.setAdapter(adapter);

        PkRSS.with(this).load("http://www.androidpro.com.br/feed/").skipCache().callback(this).async();

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //toolbar_bottom.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
         //@Override
        //public boolean onMenuItemClick(MenuItem item) {
             //Intent it = null;
             //switch (item.getItemId()) {
                 //case R.id.buscar: {
                   //  Toast.makeText(nav_drawer.this, "Adicionado ao carrinho", Toast.LENGTH_SHORT).show();
                    // break;
                // }
            // }
             //return true;
         //}
        //});

        //toolbar_bottom.inflateMenu(R.menu.menu_toolbar);


        drawerLayout = findViewById(R.id.nav_drawer);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open_drawer, R.string.close_drawer);
        drawerLayout.addDrawerListener(toggle);

        toggle.syncState();

        navigationView = findViewById(R.id.nav_drawer_view);
        navigationView.setNavigationItemSelectedListener(this);



    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_toolbar, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.buscar: {
                Toast.makeText(nav_drawer.this, "Adicionado ao carrinho", Toast.LENGTH_SHORT).show();
                break;
            }

            case R.id.favoritos: {
                Toast.makeText(nav_drawer.this, "Adicionado aos favoritos", Toast.LENGTH_SHORT).show();
                break;
            }



        }

        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.perfil: {
                Toast.makeText(this, "Perfil", Toast.LENGTH_LONG).show();
                break;
            }

            case R.id.promocao: {
                startActivity(new Intent(nav_drawer.this, MainActivity.class));
                break;
            }

            case R.id.compras: {
                Toast.makeText(this, "Compras", Toast.LENGTH_LONG).show();
                break;
            }

            case R.id.pedidos: {
                startActivity(new Intent(nav_drawer.this, recentes.class));
                break;
            }

            case R.id.lista_favoritos: {
                    Toast.makeText(this, "Lista de favoritos", Toast.LENGTH_LONG).show();
                    break;
            }

        }

        drawerLayout.closeDrawer(GravityCompat.START);

        return true;
    }

    @Override
    public void onPreload() {

    }

    @Override
    public void onLoaded(List<Article> newArticles) {
        list.clear();
        list.addAll(newArticles);
        adapter.notifyDataSetChanged();

    }

    @Override
    public void onLoadFailed() {

    }
}


