package com.example.ti.progaming;

import android.content.Intent;
import android.support.transition.Fade;
import android.support.transition.Scene;
import android.support.transition.Transition;
import android.support.transition.TransitionInflater;
import android.support.transition.TransitionManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

public class recentes extends AppCompatActivity {

    private Scene scene1;
    private Scene scene2;
    private boolean start = true;
    private Button btn;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recentes);


        final ViewGroup root = findViewById(R.id.root1);

        scene1 = Scene.getSceneForLayout(root, R.layout.activity_recentes,this);
        scene2 = Scene.getSceneForLayout(root, R.layout.activity_recentes2,this);

        imageView = findViewById(R.id.imageView3);

        btn = findViewById(R.id.ir);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TransitionManager.beginDelayedTransition(root, new Fade());

                imageView.setVisibility(imageView.getVisibility()== View.INVISIBLE ? View.VISIBLE : View.INVISIBLE);

            }
        });
    }

    public void changeScene (View view){

        Transition transition = TransitionInflater.from(this).inflateTransition(R.transition.custom);

        if(start){
            TransitionManager.go(scene2, transition);
            start=false;
        }
        else{
            TransitionManager.go(scene1 , transition);
            start = true;
        }

    }
}
