package com.example.ti.progaming;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.provider.DocumentsContract;
import android.support.design.widget.TextInputLayout;
import android.support.transition.Scene;
import android.support.transition.Transition;
import android.support.transition.TransitionInflater;
import android.support.transition.TransitionManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.text.Layout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.spark.submitbutton.SubmitButton;

import io.rmiri.buttonloading.ButtonLoading;

import static java.lang.Thread.sleep;

public class Login extends AppCompatActivity {
    private Button btnLogin;
    private Button btn;
    private ProgressBar progressBar;
    private EditText ediTextEmail;
    private EditText ediTextSenha;
    private TextInputLayout textLayoutEmail;
    private TextInputLayout textLayoutSenha;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);



        ediTextEmail =  (EditText) findViewById(R.id.txt_email);
        textLayoutEmail =  (TextInputLayout) findViewById(R.id.input_email);

        ediTextSenha = (EditText) findViewById(R.id.txt_senha);
        textLayoutSenha =  (TextInputLayout) findViewById(R.id.senha_input);

        btnLogin =  findViewById(R.id.button_login);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateForm();
            }
        });

        btn =  findViewById(R.id.cadastro);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent( Login.this,nav_drawer.class));
            }
        });
    }


    private void validateForm(){
        if (ediTextEmail.getText().toString().isEmpty()){
            textLayoutEmail.setErrorEnabled(true);
            textLayoutEmail.setError("Preencha com seu email");
        }
        else{
            textLayoutEmail.setErrorEnabled(false);
        }

      if (ediTextSenha.getText().toString().isEmpty()){
            textLayoutSenha.setErrorEnabled(true);
            textLayoutSenha.setError("Preencha com seu email");
        }
        else{
            textLayoutSenha.setErrorEnabled(false);
        }


    }


}
